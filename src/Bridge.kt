package structural
interface Switch {
    var appliance: Appliance
    fun turnOn()

}
interface Appliance {
    fun on()


}
class RemoteControl(override var appliance: Appliance) : Switch {
    override fun turnOn() = appliance.on()
}
class TV : Appliance {

    override fun on() = println("TV turned on")

}
class Radio : Appliance {
    override fun on() = println("Radio turned on")

}
fun main(args: Array<String>) {

    var tvRemoteControl = RemoteControl(appliance = TV())
    tvRemoteControl.turnOn()

    var fancyVacuumCleanerRemoteControl = RemoteControl(appliance = Radio())
    fancyVacuumCleanerRemoteControl.turnOn()


}
