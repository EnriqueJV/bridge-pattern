import Foundation

protocol Car {
    func drive()
}
class Sedan: Car {
    func drive() {
        print("Manejo un Sedan ")
    }
}
class Kia: Car {
    func drive() {
        print(" Manejo un KIA")
    }
}


protocol ColoredCar {
    var car: Car { get set }
    func drive()
}

protocol ColorblueCar {
    var car : Car {get set}
    func drive()
}

class RedCar: ColoredCar {
    var car: Car
    init(car: Car) {
        self.car = car
    }
    func drive() {
        car.drive()
        print("Es de color rojo.")
    }
}

class BlueCar: ColorblueCar {
    var car: Car
    init(car: Car) {
        self.car = car
    }
    func drive(){
        car.drive()
        print("Es de color azul")
    }
}
let sedan = Sedan()
let redSedan = RedCar(car: sedan)
redSedan.drive()

let kia = Kia()
let kiared = BlueCar(car: kia)
kiared.drive()